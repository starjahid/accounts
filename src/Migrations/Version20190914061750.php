<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190914061750 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE transaction (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', cash_transaction_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', type VARCHAR(255) NOT NULL, types_of_payment TINYINT(1) NOT NULL, date DATE NOT NULL, user_id VARCHAR(255) NOT NULL, amount INT NOT NULL, debit_or_credit TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_723705D1435DB913 (cash_transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bank_transaction (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', transaction_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', debit_or_credit TINYINT(1) NOT NULL, amount INT NOT NULL, create_at DATE NOT NULL, update_at DATE DEFAULT NULL, description VARCHAR(1000) DEFAULT NULL, bank_name VARCHAR(255) NOT NULL, account_number VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_50BCB3AE2FC0CB0F (transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cash_transaction (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', transaction_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', debit_or_credit TINYINT(1) NOT NULL, amount INT NOT NULL, create_at DATE NOT NULL, update_at DATE DEFAULT NULL, description VARCHAR(1000) NOT NULL, UNIQUE INDEX UNIQ_925602C62FC0CB0F (transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1435DB913 FOREIGN KEY (cash_transaction_id) REFERENCES transaction (id)');
        $this->addSql('ALTER TABLE bank_transaction ADD CONSTRAINT FK_50BCB3AE2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id)');
        $this->addSql('ALTER TABLE cash_transaction ADD CONSTRAINT FK_925602C62FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1435DB913');
        $this->addSql('ALTER TABLE bank_transaction DROP FOREIGN KEY FK_50BCB3AE2FC0CB0F');
        $this->addSql('ALTER TABLE cash_transaction DROP FOREIGN KEY FK_925602C62FC0CB0F');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE bank_transaction');
        $this->addSql('DROP TABLE cash_transaction');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
