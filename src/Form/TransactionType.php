<?php

namespace App\Form;

use App\Entity\Transaction;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type',null,array('attr' => ['class' => 'span11'],))
            ->add('typesOfPayment',ChoiceType::class, [
                'choices'  => [
                    'cash' => true,
                    'bank' => false,
                ],
                'expanded' => false,
                //'data' => true
                'attr' => ['class' => 'span11']
            ])
            ->add('bankName',null,array(
                'mapped' => false,'attr' => ['class' => 'span11'],
            ))
            ->add('accountNum',null,array(
                'mapped' => false,'attr' => ['class' => 'span11']
            ))
            ->add('amount',null,array('attr' => ['class' => 'span11']))
            ->add('debitOrCredit',ChoiceType::class,array('choices'  => [
                'debit' => true,
                'credit' => false,
                ],
                'expanded' => false,
                //'data' => true,
                'multiple'=> false,
            ))
            ->add('description',TextareaType::class,array(
                'mapped' => false,'attr' => ['class' => 'span11','rowspan'=>'50']
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}
