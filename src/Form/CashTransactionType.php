<?php

namespace App\Form;

use App\Entity\CashTransaction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CashTransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('debitOrCredit')
            ->add('amount')
            ->add('createAt')
            ->add('updatedAt')
            ->add('description')
            ->add('transaction')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CashTransaction::class,
        ]);
    }
}
