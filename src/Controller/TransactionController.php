<?php

namespace App\Controller;

use App\Entity\BankTransaction;
use App\Entity\CashTransaction;
use App\Entity\Transaction;
use App\Form\TransactionEditType;
use App\Form\TransactionType;
use App\Repository\CashTransactionRepository;
use App\Repository\TransactionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @Route("/transaction")
 */
class TransactionController extends AbstractController
{
    /**
     * @Route("/list", name="transaction_index", methods={"GET"})
     * @param TransactionRepository $transactionRepository
     * @return Response
     */
    public function index(TransactionRepository $transactionRepository): Response
    {


        return $this->render('transaction/index.html.twig', [
            'transactions' => $transactionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="transaction_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request): Response
    {
        $transaction = new Transaction();
        $form = $this->createForm(TransactionType::class, $transaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $transaction->setUserId("asfasd");
            print_r(new Date());
            $transaction->setDate(new \DateTime());


            $entityManager->persist($transaction);
            $entityManager->flush();



            if($transaction->getTypesOfPayment()){
                $cash = new CashTransaction();
                $cash->setAmount($transaction->getAmount());
                $cash->setDebitOrCredit($form->get('debitOrCredit')->getData());
                $cash->setDescription($form->get('description')->getData());
                $cash->setCreateAt($transaction->getDate());
                $cash->setTransaction($transaction);

                $entityManager->persist($cash);

            }
            else{

                $cash = new BankTransaction();
                $cash->setAmount($transaction->getAmount());
                $cash->setDebitOrCredit($form->get('debitOrCredit')->getData());
                $cash->setDescription($form->get('description')->getData());
                $cash->setBankName($form->get('bankName')->getData());
                $cash->setAccountNumber($form->get('accountNum')->getData());
                $cash->setCreateAt($transaction->getDate());
                $cash->setTransaction($transaction);
                $entityManager->persist($cash);

            }
            $entityManager->flush();

            return $this->redirectToRoute('transaction_index');
        }

        return $this->render('transaction/new.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transaction_show", methods={"GET"})
     * @param Transaction $transaction
     * @return Response
     */
    public function show(Transaction $transaction): Response
    {

        $data ='';
        if($transaction->getTypesOfPayment()) {
                $rep = $this->getDoctrine()->getManager()->getRepository(CashTransaction::class);
                $data = $rep->findOneBy(array('transaction'=>$transaction->getId()));

        }
        else
        {
               $rep = $this->getDoctrine()->getManager()->getRepository(BankTransaction::class);
            $data = $rep->findOneBy(array('transaction'=>$transaction->getId()));

        }


        return $this->render('transaction/show.html.twig', [
            'transaction' => $data,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="transaction_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Transaction $transaction
     * @return Response
     * @throws \Exception
     */
    public function edit(Request $request, Transaction $transaction): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(TransactionEditType::class, $transaction);
        $form->handleRequest($request);




        if ($form->isSubmitted() && $form->isValid()) {


            $data = $entityManager->getRepository(CashTransaction::class)->findOneBy(array('transaction'=>$transaction->getId()));
            if($data == null){
                $data->setBankName($form->get('bankName')->getData());
                $data->setAccountNumber($form->get('accountNum')->getData());
                $data->setAmount($transaction->getAmount());
                $data->setDebitOrCredit($form->get('debitOrCredit')->getData());
                $data->setDescription($form->get('description')->getData());
                $data->setUpdateAt(new \DateTime());
            }
            else
            {
                $data = $entityManager->getRepository(BankTransaction::class)->findOneBy(array('transaction'=>$transaction->getId()));
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('transaction_index');
        }
        else
        {

            $data = $entityManager->getRepository(CashTransaction::class)->findOneBy(array('transaction'=>$transaction->getId()));
            if($data == null){
                $data = $entityManager->getRepository(BankTransaction::class)->findOneBy(array('transaction'=>$transaction->getId()));
            }

            $form->get('description')->setData($data->getDescription());
            $form->get('debitOrCredit')->setData($transaction->getDebitOrCredit());
            $form->get('typesOfPayment')->setData($transaction->getTypesOfPayment());


            if($transaction->getTypesOfPayment()!=1)
            {
                $form->get('bankName')->setData($data->getBankName());
                $form->get('accountNum')->setData($data->getAccountNumber());
            }

        }

        return $this->render('transaction/edit.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transaction_delete", methods={"DELETE"})
     * @param Request $request
     * @param string $id
     * @return Response
     */
    public function delete(Request $request, string $id): Response
    {

        if ($this->isCsrfTokenValid('delete'.$id, $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $data = $entityManager->getRepository(CashTransaction::class)->find($id);
            if($data == null){
                $data = $entityManager->getRepository(BankTransaction::class)->find($id);
            }

            $entityManager->remove($data);
            $entityManager->flush();
        }

        return $this->redirectToRoute('transaction_index');
    }
}
